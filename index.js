const DUMMY_ARRAY = ["Dummy One", "Dummy Two"];

// Push
console.log(DUMMY_ARRAY.push("Dummy Three")); // Returns new length
console.log(DUMMY_ARRAY);

console.log("------------------------");
// Multiple Push
console.log(DUMMY_ARRAY.push("Dummy Three", "Dummy Four")); // Returns new length
console.log(DUMMY_ARRAY);

console.log("------------------------");
// Pop
console.log(DUMMY_ARRAY.pop()); // Returns removed element
console.log(DUMMY_ARRAY);

console.log("------------------------");
// Unshift
console.log(DUMMY_ARRAY.unshift("Dummy Zero")); // Returns new length
console.log(DUMMY_ARRAY);

console.log("------------------------");
// Shift
console.log(DUMMY_ARRAY.shift()); // Returns removed element
console.log(DUMMY_ARRAY);

console.log("------------------------");
// Splice --------------------(index, delete count, elementUpdate)
console.log(DUMMY_ARRAY.splice(0, 1, "The New Dummy One ")); // Returns Updated/removed element
console.log(DUMMY_ARRAY);

console.log(DUMMY_ARRAY.splice(2, 0, "Next to Dummy Two")); // Returns nothing, just adds the element to given index
console.log(DUMMY_ARRAY);

console.log("------------------------");
// Sort
console.log(DUMMY_ARRAY.sort()); // Returns sorted array
console.log(DUMMY_ARRAY.reverse()); // Returns sorted array in reverse

console.log("------------------------");
// indexOf
console.log(DUMMY_ARRAY.indexOf("Dummy Three")); // Returns index of found match
console.log(DUMMY_ARRAY.indexOf("Dummy Three", 1)); // same as above but searches from a starting index

console.log("------------------------");
// Splice
// DUMMY_ARRAY.splice(3, 0, "Dummy Three");
console.log(DUMMY_ARRAY.lastIndexOf("Dummy Three")); // Returns last matching element
console.log(DUMMY_ARRAY);

console.log("------------------------");
console.log(DUMMY_ARRAY.slice(2, DUMMY_ARRAY.length - 2)); // Makes a new array from an array. parameter is starting index, and ending index(between start and ending index)

console.log(DUMMY_ARRAY.slice(-3)); // Slice starts from the last one

console.log("------------------------");
console.log(DUMMY_ARRAY.slice(-3)); // Slice starts from the last one

console.log("------------------------");
// Concat
const DUMMY_ARRAY2 = ["Dummy Seven", "Dummy Eight"];

const COMBINED_DUMMY_ARRAY = DUMMY_ARRAY.concat(DUMMY_ARRAY2);
console.log(COMBINED_DUMMY_ARRAY);

// forEach always with a return

// every(); all elements need to meet the condition to return true. always with a return

// some(); needs at least one that matches the condition. always with a return

// Filter(); returns all that match conditions

// includes(); checks array if element exists

// reduce(); 
